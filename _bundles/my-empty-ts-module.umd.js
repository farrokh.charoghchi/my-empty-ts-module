/********   ./_bundles/my-empty-ts-module.umd.js   ********/
(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define("MyEmptyTsModule", [], factory);
	else if(typeof exports === 'object')
		exports["MyEmptyTsModule"] = factory();
	else
		root["MyEmptyTsModule"] = factory();
})(self, () => {
return /******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/core/my-empty-test-module.ts":
/*!******************************************!*\
  !*** ./src/core/my-empty-test-module.ts ***!
  \******************************************/
/***/ ((__unused_webpack_module, exports) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.MyEmptyTestModule = void 0;
var MyEmptyTestModule = /** @class */ (function () {
    function MyEmptyTestModule() {
    }
    MyEmptyTestModule.prototype.hello = function (name) {
        console.log('Hello ' + name + '!');
    };
    return MyEmptyTestModule;
}());
exports.MyEmptyTestModule = MyEmptyTestModule;


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
var exports = __webpack_exports__;
/*!**********************!*\
  !*** ./src/index.ts ***!
  \**********************/

Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.MyEmptyTestModule = void 0;
var my_empty_test_module_1 = __webpack_require__(/*! ./core/my-empty-test-module */ "./src/core/my-empty-test-module.ts");
Object.defineProperty(exports, "MyEmptyTestModule", ({ enumerable: true, get: function () { return my_empty_test_module_1.MyEmptyTestModule; } }));

})();

/******/ 	return __webpack_exports__;
/******/ })()
;
});
//# sourceMappingURL=my-empty-ts-module.umd.js.map

