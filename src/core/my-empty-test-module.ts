class MyEmptyTestModule{
    hello(name: string) {
        console.log('Hello '+ name + '!');
    }
}

export {
    MyEmptyTestModule
}